package com.example.composecocktailapp

import android.app.Application
import com.example.composecocktailapp.koin.cocktailModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


class KoinApplication: Application(){
    override fun onCreate() {
        super.onCreate()
        startKoin{
            androidContext(this@KoinApplication)
            modules(cocktailModule)
        }
    }
}