package com.example.composecocktailapp.model.response.details


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class DetailsDTO(
    @SerialName("drinks")
    val drinks: List<DrinkDetails>
)