package com.example.composecocktailapp.model.response.details


import kotlinx.serialization.Contextual
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class DrinkDetails(
    @SerialName("dateModified")
    val dateModified: String,
    @SerialName("idDrink")
    val idDrink: String,
    @SerialName("strAlcoholic")
    val strAlcoholic: String,
    @SerialName("strCategory")
    val strCategory: String,
    @SerialName("strCreativeCommonsConfirmed")
    val strCreativeCommonsConfirmed: String,
    @SerialName("strDrink")
    val strDrink: String,
    @SerialName("strDrinkAlternate")
    @Contextual
    val strDrinkAlternate: Any,
    @SerialName("strDrinkThumb")
    val strDrinkThumb: String,
    @SerialName("strGlass")
    val strGlass: String,
    @SerialName("strIBA")
    val strIBA: String,
    @SerialName("strImageAttribution")
    val strImageAttribution: String,
    @SerialName("strImageSource")
    val strImageSource: String,
    @SerialName("strIngredient1")
    val strIngredient1: String,
    @SerialName("strIngredient10")
    @Contextual
    val strIngredient10: Any,
    @SerialName("strIngredient11")
    @Contextual
    val strIngredient11: Any,
    @SerialName("strIngredient12")
    @Contextual
    val strIngredient12: Any,
    @SerialName("strIngredient13")
    @Contextual
    val strIngredient13: Any,
    @SerialName("strIngredient14")
    @Contextual
    val strIngredient14: Any,
    @SerialName("strIngredient15")
    @Contextual
    val strIngredient15: Any,
    @SerialName("strIngredient2")
    val strIngredient2: String,
    @SerialName("strIngredient3")
    val strIngredient3: String,
    @SerialName("strIngredient4")
    val strIngredient4: String,
    @SerialName("strIngredient5")
    @Contextual
    val strIngredient5: Any,
    @SerialName("strIngredient6")
    @Contextual
    val strIngredient6: Any,
    @SerialName("strIngredient7")
    @Contextual
    val strIngredient7: Any,
    @SerialName("strIngredient8")
    @Contextual
    val strIngredient8: Any,
    @SerialName("strIngredient9")
    @Contextual
    val strIngredient9: Any,
    @SerialName("strInstructions")
    val strInstructions: String,
    @SerialName("strInstructionsDE")
    val strInstructionsDE: String,
    @SerialName("strInstructionsES")
    @Contextual
    val strInstructionsES: Any,
    @SerialName("strInstructionsFR")
    @Contextual
    val strInstructionsFR: Any,
    @SerialName("strInstructionsIT")
    val strInstructionsIT: String,
    @SerialName("strInstructionsZH-HANS")
    @Contextual
    val strInstructionsZHHANS: Any,
    @SerialName("strInstructionsZH-HANT")
    @Contextual
    val strInstructionsZHHANT: Any,
    @SerialName("strMeasure1")
    val strMeasure1: String,
    @SerialName("strMeasure10")
    @Contextual
    val strMeasure10: Any,
    @SerialName("strMeasure11")
    @Contextual
    val strMeasure11: Any,
    @SerialName("strMeasure12")
    @Contextual
    val strMeasure12: Any,
    @SerialName("strMeasure13")
    @Contextual
    val strMeasure13: Any,
    @SerialName("strMeasure14")
    @Contextual
    val strMeasure14: Any,
    @SerialName("strMeasure15")
    @Contextual
    val strMeasure15: Any,
    @SerialName("strMeasure2")
    val strMeasure2: String,
    @SerialName("strMeasure3")
    val strMeasure3: String,
    @SerialName("strMeasure4")
    @Contextual
    val strMeasure4: Any,
    @SerialName("strMeasure5")
    @Contextual
    val strMeasure5: Any,
    @SerialName("strMeasure6")
    @Contextual
    val strMeasure6: Any,
    @SerialName("strMeasure7")
    @Contextual
    val strMeasure7: Any,
    @SerialName("strMeasure8")
    @Contextual
    val strMeasure8: Any,
    @SerialName("strMeasure9")
    @Contextual
    val strMeasure9: Any,
    @SerialName("strTags")
    val strTags: String,
    @SerialName("strVideo")
    @Contextual
    val strVideo: Any
)