package com.example.composecocktailapp.model.response.routes

object Routes {
    private const val BASE_URL = "https://www.thecocktaildb.com/api/json/v1/1"
    const val CATEGORY = "$BASE_URL/list.php"
    const val DRINKS = "$BASE_URL/filter.php"
    const val DETAILS = "$BASE_URL/lookup.php"
    const val QUERY_C = "c"
    const val QUERY_I = "i"
}