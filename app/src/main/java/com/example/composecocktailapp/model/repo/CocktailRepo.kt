package com.example.composecocktailapp.model.repo

import com.example.composecocktailapp.model.service.CocktailServiceImpl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CocktailRepo(private val cocktailServiceImpl: CocktailServiceImpl) {
    suspend fun getCategories() = withContext(Dispatchers.IO) {
        cocktailServiceImpl.getCategories()
    }
    suspend fun getDrinks(category: String) = withContext(Dispatchers.IO){
        cocktailServiceImpl.getDrinks(category)
    }
    suspend fun getDetails(id: String) = withContext(Dispatchers.IO) {
        cocktailServiceImpl.getDetails(id)
    }
}