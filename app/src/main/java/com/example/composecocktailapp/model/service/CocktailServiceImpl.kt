package com.example.composecocktailapp.model.service

import com.example.composecocktailapp.model.response.category.CategoryDTO
import com.example.composecocktailapp.model.response.details.DetailsDTO
import com.example.composecocktailapp.model.response.drinks.DrinksDTO
import com.example.composecocktailapp.model.response.routes.Routes
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.parameter
import io.ktor.client.statement.HttpResponse

class CocktailServiceImpl(private val client: HttpClient) : CocktailService {
    suspend fun getCategories(): CategoryDTO {
        try {
        val response : HttpResponse = client.get(Routes.CATEGORY)
        return response.body()
        } catch (ex: Exception) {
            ex.localizedMessage
            throw ex
        }
    }

    suspend fun getDrinks(category: String): DrinksDTO {
        try {
            val response: HttpResponse = client.get(Routes.DRINKS) {
                parameter(Routes.QUERY_C, category)
            }
            return response.body()
        } catch (ex: Exception) {
            ex.localizedMessage
            throw ex
        }
    }

    suspend fun getDetails(id: String): DetailsDTO {
        try {
            val response: HttpResponse = client.get(Routes.DETAILS) {
                parameter(Routes.QUERY_I, id)
            }
            return response.body()
        } catch (ex: Exception) {
            ex.localizedMessage
            throw ex
        }
    }
}