package com.example.composecocktailapp.model.response.category


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class DrinkCategory(
    @SerialName("strCategory")
    val strCategory: String
)