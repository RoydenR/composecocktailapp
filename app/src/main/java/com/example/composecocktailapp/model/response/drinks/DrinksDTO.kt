package com.example.composecocktailapp.model.response.drinks


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class DrinksDTO(
    @SerialName("drinks")
    val drinks: List<CategoryDrinks>
)