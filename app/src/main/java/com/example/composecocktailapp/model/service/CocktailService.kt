package com.example.composecocktailapp.model.service

import io.ktor.client.HttpClient

interface CocktailService {
    companion object {
        fun createCocktailService(): CocktailServiceImpl {
            return CocktailServiceImpl(client = HttpClient())
        }
    }
}