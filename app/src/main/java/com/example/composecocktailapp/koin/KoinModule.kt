package com.example.composecocktailapp.koin

import com.example.composecocktailapp.model.repo.CocktailRepo
import com.example.composecocktailapp.model.service.CocktailService
import com.example.composecocktailapp.model.service.CocktailServiceImpl
import io.ktor.client.HttpClient
import io.ktor.client.engine.android.Android
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.json.Json
import org.koin.dsl.module

val cocktailModule = module {
    single { CocktailService.createCocktailService() }
    single { CocktailRepo(get()) }
    single { CocktailServiceImpl(client = HttpClient()) }
    single {
        HttpClient(Android) {
            install(ContentNegotiation) {
                json(Json {
                    ignoreUnknownKeys = true
                    isLenient = true
                    prettyPrint = true
                })
            }
        }
    }
}